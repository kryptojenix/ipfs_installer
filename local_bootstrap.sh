#!/bin/bash
ipfs bootstrap rm --all

# load bootstrap from file 
#cat ~/.ipfs/local_bootstrap.save | ipfs bootstrap add

# load bootstrap manually

ipfs bootstrap add /ip4/192.168.3.20/tcp/4001/ipfs/QmbJbHbmtaBqqkwU1GZUfVhif4VUcwLWUuFCNxY9E1rQXb
ipfs bootstrap add /ip4/192.168.3.21/tcp/4001/ipfs/QmSZNQ9Q2EAD1P23FnhCcajiv4Lbstpc6yLrL973f51MY6
ipfs bootstrap add /ip4/192.168.3.22/tcp/4001/ipfs/QmWwAjGz6kYPTdVcfgG3EuJKvsbxGhSiKTj5KvtHSgXLmf
ipfs bootstrap add /ip4/192.168.3.23/tcp/4001/ipfs/QmceQPbbtcFgdYFLUzTC8YoQBjGfMkgQPdGBzoTq8P7Eyj

ipfs bootstrap list > ~/.ipfs/local_bootstrap.save

