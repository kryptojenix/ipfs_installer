#!/bin/bash

# guess the OS

# scrape this page for the links
# https://dist.ipfs.io/#go-ipfs


dataFolder="/home/$USER/ipfsDATA"
currentVersionLink="https://dist.ipfs.io/go-ipfs/v0.4.22/go-ipfs_v0.4.22_linux-amd64.tar.gz"
currentVersionFilename="~/Downloads/go-ipfs_v0.4.22_linux-amd64.tar.gz"
# check for the service
if [ ! -f /lib/systemd/system/ipfs.service ]; then
    # TODO: substitute local values into the service file
    echo"you will need to edit /lib/systemd/system/ipfs.service"
    sudo cp ./ipfs.service /lib/systemd/system/

    
    # check for previous installation
    if [ -d $dataFolder ]; then
        echo "found previous data folder"
        ipfsVersion=$(ipfs --version)
        echo "IPFS version is $ipfsVersion"
    else
        if [ ! -d ~/.ipfs ]; then
            echo "Installing IPFS"
            if [ ! -d $currentVersionFilename ]; then
                echo "Downloading $currentVersionLink"
                wget $currentVersionLink ~/Downloads/
            else
                tar xvfz $currentVersionFilename
                sudo ./go-ipfs/install.sh
                echo "IPFS installation finished"

            echo "initialising this node"
            ipfs init
            ipfsID=$(ipfs id)
            echo "ID is $ipfsID"
        else
            echo "Installation found"
            ipfs --version
            echo "Creating the data folder $dataFolder"
            mkdir $dataFolder
        fi
    
    # start the service
    
    fi
else
    # check the service is running
    status=$(systemctl status ipfs.service)
    echo $status
fi


# read local_bootstrap.sh to get the list of peers


